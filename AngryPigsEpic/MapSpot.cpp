#include "MapSpot.h"


void MapSpot::AddOutput(MapSpot * output){
	if (_outputCount == 0){
		_output = new MapSpot*[1];
		_outputCount = 1;
		_output[0] = output;
	}
	else{

		MapSpot ** tmpOutput = _output;

		_output = new MapSpot*[_outputCount + 1];
		for (int i = 0; i < _outputCount; i++)
		{
			_output[i] = tmpOutput[i];
		}
		_output[_outputCount] = output;
		delete[] tmpOutput;
		_outputCount++;
	}
}

MapSpot::MapSpot(Position p, MapSpot * input, string name) :_position(p)
{
	_output = nullptr;
	_input = input;
	_active = false;
	_completed = false;
	_name = name;

	if (input != nullptr)
		input->AddOutput(this);
}

MapSpot::MapSpot(Position p, MapSpot * input, string name, MapSpot ** output, int outputCount) :
_position(p), _input(input), _name(name), _outputCount(outputCount), _active(false), _completed(false)
{
	_output = _output = new MapSpot*[_outputCount];
	for (int i = 0; i < _outputCount; i++)
	{
		_output[i] = output[i];
	}
}

MapSpot::MapSpot(const MapSpot & other)
{
	_outputCount = other._outputCount;
	_output = new MapSpot*[_outputCount];

	for (int i = 0; i < _outputCount; i++)
	{
		_output[i] = other._output[i];
	}
	_input = other._input;
	_active = other._active;
	_completed = other._completed;
	_name = other._name;
}

MapSpot::~MapSpot()
{
	if (_output != nullptr){
		delete[]  _output;
		_outputCount = 0;
		_output = nullptr;
	}
}

Position MapSpot::getPosition() const
{
	return _position;
}

void MapSpot::setPosition(const Position & newPosition)
{
	_position = newPosition;
}

string MapSpot::getName() const
{
	return _name;
}

void MapSpot::setName(string value)
{
	_name = value;
}

bool MapSpot::isActive() const
{
	return _active;
}

bool MapSpot::isCompleted() const
{
	return _completed;
}

void MapSpot::setActive(bool value)
{
	_active = value;
}

void MapSpot::setCompleted(bool value)
{
	_completed = value;
}

MapSpot* MapSpot::getOutput(int index){

	return _output[index];
}

MapSpot& MapSpot::operator=(const MapSpot & other)
{
	delete[] _output;

	_outputCount = other._outputCount;
	_output = new MapSpot*[_outputCount + 1];

	for (int i = 0; i < _outputCount; i++)
	{
		_output[i] = other._output[i];
	}
	_input = other._input;
	_active = other._active;
	_completed = other._completed;

	return *this;
}
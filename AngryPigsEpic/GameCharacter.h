#pragma once
#include "GameObject.h"
#include "AtackResult.h"

class GameCharacter :
	public GameObject
{
	int _level;
	int _health;
	int _atack;
	int _defense;
	GameObject *_weapon;
	GameObject *_armor;
	GameObject *_shield;

public:
	GameCharacter();
	virtual ~GameCharacter();

	virtual void Atack(GameCharacter * characterToAtack);
	virtual AtackResult RunMagicSkill(GameCharacter * characterToAtack);
	virtual void UsePotion(GameObject *);

	virtual void SetHealth(int value);
	virtual void SetLevel(int value);
	virtual void SetAtack(int value);
	virtual void SetDefense(int value);
	virtual void SetWeapon(GameObject* value);
	virtual void SetArmor(GameObject* value);
	virtual void SetShield(GameObject* value);
	
	virtual int GetHealth() const;
	virtual int GetLevel() const;
	virtual int GetAtack() const;
	virtual int GetDefense() const;
	virtual GameObject* GetWeapon() const;
	virtual GameObject* GetArmor() const;
	virtual GameObject* GetShield() const;

};


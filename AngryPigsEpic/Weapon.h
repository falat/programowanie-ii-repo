#pragma once
#include "GameCharacter.h"

__interface IAtackResult
{
public:
	 int getAtackValue() const;
	 int getAtackResoponseValue() const ;
};
#define implements public

class DefaultAtackResult : implements IAtackResult
{
protected:
	int _atackValue;
public:
	DefaultAtackResult(int atackValue);
	int getAtackValue() const;
	int getAtackResoponseValue() const;
};


class Weapon : GameObject
{
protected:
	int _weaponAtackValue;
	int _weaponDefenseValue;
public:
	Weapon(int weaponAtackValue ,int weaponDefenseValue);

	virtual ~Weapon();
	virtual IAtackResult* Atack(GameCharacter * character) = 0;
	virtual int getAtackValue() const;
	virtual int getDefenceValue() const;
};


class Sword : public Weapon{
public:
	virtual IAtackResult* Atack(GameCharacter * character);
};
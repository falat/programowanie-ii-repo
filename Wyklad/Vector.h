#pragma once
#include "Matrix.h"
class Vector :
	public Matrix
{
public:
	Vector(int elementsCount);
	Vector(const Vector& other);
	Vector& operator=(const Vector& other);
	virtual ~Vector();
    double& operator[](int i) ;
	double& operator()(int i);
};


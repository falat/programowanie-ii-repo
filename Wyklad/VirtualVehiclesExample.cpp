#include "VirtualVehiclesExample.h"

//////////////////////////////////////////////////////////////
LandVehicle_Virtual::LandVehicle_Virtual(int x, int y, int seatsNumber) :
Vehicle(x, y)
{
	cout << " Land Vehicle Virtual- construktor " << endl;
}
LandVehicle_Virtual::~LandVehicle_Virtual(){}

//////////////////////////////////////////////////////////////
WaterWehicle_Virtual::WaterWehicle_Virtual(int x, int y) :
Vehicle(x, y)
{
	cout << " Water Vehicle Virtual- construktor " << endl;
}
WaterWehicle_Virtual::~WaterWehicle_Virtual(){};

//////////////////////////////////////////////////////////////
Car_Virtual::Car_Virtual(int x, int y, int seatsNuber, string color) :
Vehicle(x, y),
LandVehicle_Virtual(x, y, seatsNuber)
{
	cout << " Car Virtual- construktor " << endl;
}

Car_Virtual::~Car_Virtual(){};
void Car_Virtual::Display() const{
	cout << "Car Virtual " << _x << " " << _y << endl;
}

//////////////////////////////////////////////////////////////
Boat_Virtual::Boat_Virtual(int x, int y) :
Vehicle(x, y),
WaterWehicle_Virtual(x, y)
{
	cout << " Boat Virtual- construktor " << endl;
}
Boat_Virtual::~Boat_Virtual(){}
 void Boat_Virtual::Display() const{
	 cout << "Boat Virtual " << _x << " " << _y << endl;
}

//////////////////////////////////////////////////////////////
Amphibia_Virtual::Amphibia_Virtual(int x, int y, int seatsNumber) :
Vehicle(x, y),
LandVehicle_Virtual(x, y, seatsNumber),
WaterWehicle_Virtual(x, y),
Car_Virtual(x, y, seatsNumber, "green"),
Boat_Virtual(x, y)
{
	cout << " Amphibia Virtual- construktor " << endl;
}

Amphibia_Virtual::~Amphibia_Virtual()
{
}

void Amphibia_Virtual::Display() const{
	cout << "Amphibia virtual at: " << _x << " " << _y << endl;
}

//////////////////////////////////////////////////////////////
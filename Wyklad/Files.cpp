// Tekstowe
#include <iostream>
#include <fstream>

using namespace std;


void TextFilesExample(){
	cout << "--------TextFilesExample----------" << endl;
	ofstream out;
	out.open("fileexample.txt");

	out << " zapis do pliku tekstowego" << endl;

	out.close();

	ifstream input("fileexample.txt");
	char buffer[255];

	input >> buffer;

	cout << buffer << endl;
	input.getline(buffer, 255, '\n');

	cout << buffer << endl;
	input.close();
}

void TextFilesWithValuesExample(){
	cout << "--------TextFilesWithValuesExample----------" << endl;

	ofstream out;

	out.open("fileexample.txt");


	double val1 = 3.45, val1in = 0;
	double val2 = 312.33, val2in = 0;
	double val3 = 23.43, val3in;

	out << " zapis liczb do pliku tekstowego" << endl;
	out << val1 << " " << val2 << "\t" << val3 << endl;

	out.close();

	ifstream input("fileexample.txt");
	char buffer[255];

	input.getline(buffer, 255, '\n');
	input >> val1in >> val2in >> val3in;

	cout << val1in << " " << val2in << " " << val3in << endl;
	input.close();

}
void AppendTextFileExample(){
	cout << "--------AppendTextFileExample----------" << endl << "\t";
	TextFilesExample();

	ofstream out;
	out.open("fileexample.txt", ios::out | ios::app | ios::ate);

	out << "Dopisane do pliku" << endl;

	ifstream input("fileexample.txt");
	char buffer[255];

	input.getline(buffer, 255, '\n');
	cout << "\t" << buffer << endl;

	input.getline(buffer, 255, '\n');
	cout << "\t" << buffer << endl;

	input.close();
}

void TruncateFileExample(){

}

struct Dane{
	int id;
	char name[30];
	double value;
};



void BinnaryDataExample(){
	cout << "--------BinnaryDataExample----------" << endl << "\t";
	TextFilesExample();

	ofstream out;
	out.open("binaryfileexample.bin", ios::out | ios::binary | ios::trunc);
	Dane outData;
	outData.id = 1;
	outData.value = 10.3;

	strcpy_s(outData.name, "dane testowe");

	out.write((char*)&outData, sizeof(outData));

	out.close();
	ifstream input("binaryfileexample.bin");
	Dane inputData;
	input.read((char*)&inputData, sizeof(Dane));
	cout << "\t" << "Id=" << inputData.id << " value=" << inputData.value << " Name=" << inputData.name << endl;

	input.close();
}

void BinnaryDatabaseExample(){

	cout << "--------BinnaryDatabaseExample----------" << endl << "\t";
	TextFilesExample();

	ofstream out;
	out.open("binaryfileexample.bin", ios::out | ios::binary | ios::trunc);
	Dane outData;
	strcpy_s(outData.name, "dane testowe");
	for (int i = 0; i < 10; i++)
	{
		outData.id = i;
		outData.value = i*10.3;
		out.write((char*)&outData, sizeof(outData));
	}

	out.close();
	ifstream input("binaryfileexample.bin");
	Dane inputData;
	while (input.read((char*)&inputData, sizeof(Dane)))
	{
		cout << "\t" << "Id=" << inputData.id << " value=" << inputData.value << " Name=" << inputData.name << endl;
	}

	input.close();
}

void SeekExample(){
	cout << "--------SeekExample----------" << endl;

	ofstream out;
	out.open("binaryfileexample.bin", ios::out | ios::binary | ios::trunc);
	Dane outData;
	strcpy_s(outData.name, "dane testowe");
	for (int i = 0; i < 10; i++)
	{

		outData.id = i;
		outData.value = i*10.3;
		out.write((char*)&outData, sizeof(outData));
	}

	out.close();
	ifstream input("binaryfileexample.bin");
	Dane inputData;
	for (int i = 0; i < 10; i += 2)
	{
		input.seekg(i*sizeof(Dane), ios::beg); // ios::cur  ios::end
		input.read((char*)&inputData, sizeof(Dane));
		cout << "\t" << "Id=" << inputData.id << " value=" << inputData.value << " Name=" << inputData.name << endl;
	}

	input.close();
}


void FstreamExample(){
	cout << "--------FstreamExample----------" << endl;

	fstream stream;
	stream.open("binaryfileexample.bin", ios::out | ios::in | ios::binary | ios::trunc);
	Dane outData;
	strcpy_s(outData.name, "dane testowe");
	Dane inputData;
	for (int i = 0; i < 10; i++)
	{
		outData.id = i;
		outData.value = i*10.3;
		stream.seekp(i*sizeof(Dane), ios::beg); // ios::cur  ios::end
		stream.write((char*)&outData, sizeof(outData));

		stream.seekg(i*sizeof(Dane), ios::beg);
		stream.read((char*)&inputData, sizeof(Dane));
		cout << "\t" << "Id=" << inputData.id << " value=" << inputData.value << " Name=" << inputData.name << endl;
	}

	stream.close();
}

class Person{
public:
	int id;
	char firstName[80];
	char lastName[90];
	Person(int id, char firstName[], char lastName[]){
		this->id = id;
		strcpy_s(this->firstName, firstName);
		strcpy_s(this->lastName, lastName);
	}
};

class Managment{
public:

	Person *Chairman;
	Person *ViceChairman[2];
	Person* Members[10];
	char CompanyName[100];
};
void PointersWithFillesExample(){
	
	cout << "--------PointersWithFillesExample----------" << endl; 
	Managment outData;


	ofstream out;
	out.open("binaryfileexample.bin", ios::out | ios::binary | ios::trunc);


	outData.Chairman = new Person(1, "Jozef", "Tkaczuk");
	outData.ViceChairman[0] = new Person(2, "Jan", "Kowalski");
	outData.ViceChairman[1] = new Person(3, "Andrzej", "Nowak");
	strcpy_s(outData.CompanyName, "Testowa Firma");

	out.write((char*)&outData, sizeof(outData));

	out.close();
	ifstream input("binaryfileexample.bin");
	Managment inputData;

	input.read((char*)&inputData, sizeof(Managment));
	cout << "\t Pulapka bo wygl�da, ze dziala dobrze" << endl;
	cout << "\t" << "Id=" << inputData.CompanyName << " chairman=" << inputData.Chairman->firstName << " Name=" << inputData.Chairman->lastName << endl;
	
	input.close();

	delete outData.Chairman;
	delete outData.ViceChairman[0];
	delete outData.ViceChairman[1];

	input.open("binaryfileexample.bin");
	input.read((char*)&inputData, sizeof(Managment));
	cout << "\t Teraz widac ze dziala zle" << endl;
	cout << "\t" << "Id=" << inputData.CompanyName << " chairman=" << inputData.Chairman->firstName << " Name=" << inputData.Chairman->lastName << endl;

	input.close();
}

void SerializationExample(){

}

void FilesExamples(){

	TextFilesExample();
	/*TextFilesWithValuesExample();
	AppendTextFileExample();
	BinnaryDatabaseExample();
	SeekExample();
	FstreamExample();

	PointersWithFillesExample();*/
}
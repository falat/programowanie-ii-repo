#pragma once
#include <iostream>
using namespace std;

class Shape
{
public:
		static string shapTypeName;
		static string GetClassName();
protected:
	string _shapeName;
public:
	Shape(string shapeName);

	virtual ~Shape();
	
	virtual double Area() const;
	virtual double Perimeter() const; // obw�d

	friend ostream& operator<<(ostream& out, const Shape & other);
	friend ostream& operator<<(ostream& out, const Shape * other);
};

class Rectangle: public Shape
{
	double _a, _b;
public:
	Rectangle(double a, double b);
	Rectangle(const Rectangle &other);
	virtual ~Rectangle();
	virtual Rectangle & operator=(const Rectangle &other);
	virtual double Area() const;
	virtual double Perimeter() const; 
	static string GetClassName();
	//friend ostream& operator<<(ostream& out, const Rectangle & other);
	//friend ostream& operator<<(ostream& out, const Rectangle * other);
};

class Circle : public Shape
{
	double _r;
public:
	Circle(double r);

	virtual ~Circle();

	virtual double Perimeter() const;

	//friend ostream& operator<<(ostream& out, const Circle & other);
	//friend ostream& operator<<(ostream& out, const Circle * other);
};


class Square: public Shape
{

};
#pragma once

#include <iostream>

using namespace std;

template<class Type>
class ATHArray
{
protected:
	Type * _array;
	int _count;
public:

	ATHArray(int n)
	{
		_count = n;
		_array = new Type[n];
		for (int i = 0; i < _count; i++)
		{
			_array[i] = 0;
		}
	}

	ATHArray(const ATHArray<Type> & other)
	{
		_count = other._count;
		_array = new Type[n];
		for (int i = 0; i < _count; i++)
		{
			_array[i] = other._array[i];
		}
	}

	virtual ~ATHArray()
	{
		if (_array != nullptr)
			delete[]_array;
		_array = nullptr;
	}

	virtual ATHArray& operator = (const ATHArray<Type> & other)
	{
		this->~ATHArray();

		_count = other._count;
		_array = new Type[_count];
		for (int i = 0; i < _count; i++)
		{
			_array[i] = other._array[i];
		}

		return *this;
	}
	virtual Type & operator[](int i){
		if (i < 0 || i >= _count) throw "Index Out of Range";
		return _array[i];
	}
	virtual int getCount() const{
		return _count;
	}
};
//template<class Type>
//int ATHArray<Type>::getCount() const{
//	return _count;
//}


template<class Type>
class SizedArray : public ATHArray < Type >
{
protected:
	int _topBound;
	int _lowBound;
public:
	SizedArray(int lowBound, int topBound) : ATHArray<Type>(topBound - lowBound){
		_topBound = topBound;
		_lowBound = lowBound;
	}
	SizedArray(const ATHArray<Type> & other) : ATHArray<Type>(other){
		_topBound = other._topBound;
		_lowBound = other._lowBound;
		for (int i = 0; i < _count; i++)
		{
			_array[i] = other._array[i];
		}
	}
	virtual  ~SizedArray(){
	}

	virtual SizedArray<Type>& operator = (const ATHArray<Type> & other){
		this->ATHArray<Type>:: operator=(other);

		return *this;
	}
	virtual Type & operator[](int i){
		if (i < _lowBound || i >= _topBound) throw "Index Out of Range";

		return ATHArray<Type>:: operator[](i - _lowBound);
	}
	virtual int getLowBound() const{
		return _lowBound;
	}
	virtual int getTopBound() const{
		return _topBound;
	}
};

//template <class Type>
//ostream& operator<<(ostream & out, const Type & a){
//	for (int i = 0; i < a.getCount(); i++)
//	{
//		out << a[i] << end;
//	}
//}

template <class Type>
ostream& operator<<(ostream & out, ATHArray<Type> & a){
	for (int i = 0; i < a.getCount(); i++)
	{
		out << a[i] << endl;
	}

	return out;
}

template <class Type>
ostream& operator<<(ostream & out, SizedArray<Type> & a){
	for (int i = a.getLowBound(); i < a.getTopBound(); i++)
	{
		out << a[i] << endl;
	}

	return out;
}

//template <class Type>
//Type max(Type val1, Type val 2){
//	return val1 > val2 ? val1 : val2;
//}
//
//template <class Type1, class Type2>
//bool equals(Type1 val1, Type2 val 2){
//	return val1 == val2;
//}

template <class ElementType, class IdType>
class SizedArrayWithId : public SizedArray < ElementType >
{
protected:
	IdType _id;

public:
	SizedArrayWithId(int lowBound, int topBound, IdType id) : SizedArray(lowBound, topBound){
		_id = id;
	}
	SizedArrayWithId(const SizedArrayWithId<ElementType, IdType> & other) : SizedArray(other){
		_id = other._id;
	}

	virtual IdType getId() { return _id; }
};